﻿using System;
using System.Net.Mail;
using System.Xml.Serialization;

namespace ExcentOne.Core.Email.Data
{
    [Serializable]
    public class Email
    {
        public Email()
        {
            To = string.Empty;
            Cc = string.Empty;
            Bcc = string.Empty;
            ToCollection = new MailAddressCollection();
            CcCollection = new MailAddressCollection();
            BccCollection = new MailAddressCollection();
            From = string.Empty;
        }

        [XmlIgnore] public string To { get; set; }
        [XmlIgnore] public string Cc { get; set; }
        [XmlIgnore] public string Bcc { get; set; }

        [XmlIgnore] public MailAddressCollection ToCollection { get; set; }

        [XmlIgnore] public MailAddressCollection CcCollection { get; set; }

        [XmlIgnore] public MailAddressCollection BccCollection { get; set; }

        [XmlIgnore] public string From { get; set; }

        [XmlIgnore] public string FromName { get; set; }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsHtml { get; set; }
    }
}